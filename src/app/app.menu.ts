import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { NavegacionComponent } from './navegacion/navegacion.component';
import { ProductosComponent } from './productos/productos.component';
import { CrearproductoComponent } from './crearproducto/crearproducto.component';
import { ListarproductoComponent } from './listarproducto/listarproducto.component';
import { AgregarproductoComponent } from './agregarproducto/agregarproducto.component';
import { EditarComponent } from './editar/editar.component';
import { UnicoComponent } from './unico/unico.component';


const app_routes: Routes = [
  { path: 'inicio', component: InicioComponent },
  { path: 'navegacion', component: NavegacionComponent },
  { path: 'productos/editar/:id', component: EditarComponent },
  { path: 'productos/unico/:id', component: UnicoComponent },
  { path: 'productos', component: ProductosComponent },
  { path: 'crear', component: CrearproductoComponent },
  { path: 'crear/listar_productos', component: ListarproductoComponent },
  { path: 'crear/agregar_productos', component: AgregarproductoComponent },
  { path: '**', pathMatch: 'full', redirectTo: '' }
];

export const Menu = RouterModule.forRoot(app_routes);
