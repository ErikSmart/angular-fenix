import { Component, OnInit } from '@angular/core';
import { Producto } from '../models/producto';
import { ProductosService } from '../services/productos.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-crearproducto',
  templateUrl: './crearproducto.component.html',
  styleUrls: ['./crearproducto.component.css']
})
export class CrearproductoComponent implements OnInit {
public nombre: string;
public producto: Producto;

  constructor(private productosService: ProductosService, private activatedRoute: ActivatedRoute, private router: Router) {  }

  ngOnInit() {
  }

}
