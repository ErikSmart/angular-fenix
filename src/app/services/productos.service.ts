import { Injectable } from '@angular/core';

//Para llamar Http en Agular de 5 a 8 usar HttpClient (no olvidar poner en app Module import { HttpClientModule } from '@angular/common/http';)
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { Producto } from '../models/producto';
import { GLOBAL } from './global';
//agregar npm install --save rxjs-compat
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {
  public url: string;

  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  //Angular 2-4 return this.http.get('url').map((res: Response) => res.json());
  //Nueva forma de llamar fetch en angular 5 al 8
  getProducto() {
    return (this.http.get(this.url));
  }
  //Producto es el objeto que llega del Form
  guardarServicioProducto(producto: Producto)
  {

  /*  return this.http.post(this.url + 'dto', producto, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).map(data => data);
*/
    let json = JSON.stringify(producto);
    let params = json;
    let headers = {headers: new  HttpHeaders({ 'Content-Type': 'application/json'})};

    return this.http.post(this.url+'dto', producto, headers).map(res => res);

  }
// Producto individual
  getDetalle(id){
    return (this.http
      .get(this.url+id));
  }

  // Actulizar producto

  actulizarProducto(id, producto: Producto)
  {

    let headers = {headers: new  HttpHeaders({ 'Content-Type': 'application/json'})};
    return this.http.put(this.url+id, producto, headers).map(res => {res});
  }



}
