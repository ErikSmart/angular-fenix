import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProductosService } from '../services/productos.service';
import { Producto } from '../models/producto';

@Component({
  selector: 'app-unico',
  templateUrl: './unico.component.html',
  styleUrls: ['./unico.component.css']
})
export class UnicoComponent implements OnInit {
public id: any;
public unico: Producto;
  constructor(private productosService: ProductosService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.getDetalle();
  }

  getDetalle()
  {
    //para recorrer los parametros y asignarlo al id
      this.activatedRoute.params.forEach((params: Params) => {
        let id = params['id'];
        this.productosService.getDetalle(id)
        .subscribe(arg => {this.unico = <any>arg});
      });

  }

}
