import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Menu } from './app.menu';

import { HttpClientModule } from '@angular/common/http';


import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InicioComponent } from './inicio/inicio.component';
import { NavegacionComponent } from './navegacion/navegacion.component';
import { ProductosComponent } from './productos/productos.component';
import { CrearproductoComponent } from './crearproducto/crearproducto.component';
import { AgregarproductoComponent } from './agregarproducto/agregarproducto.component';
import { ListarproductoComponent } from './listarproducto/listarproducto.component';
import { ProductosService } from './services/productos.service';
import { EditarComponent } from './editar/editar.component';
import { UnicoComponent } from './unico/unico.component';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    NavegacionComponent,
    ProductosComponent,
    CrearproductoComponent,
    AgregarproductoComponent,
    ListarproductoComponent,
    EditarComponent,
    UnicoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    Menu,
    HttpClientModule,
    FormsModule
  ],
  providers: [ProductosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
