import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProductosService } from '../services/productos.service';
import { Producto } from '../models/producto';



@Component({
  selector: 'app-verproducto',
  templateUrl: './listarproducto.component.html',
  styleUrls: ['./listarproducto.component.css']
})
export class ListarproductoComponent implements OnInit {
public producto: Producto[];
  constructor(private _route: ActivatedRoute, private _router: Router, private productosService: ProductosService) { }

  ngOnInit() {
    this.productosService.getProducto().subscribe(re => {this.producto = <any>re});
  }

}
