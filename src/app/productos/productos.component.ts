import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProductosService } from '../services/productos.service';
import { Producto } from '../models/producto';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {
public producto : Producto[];
  constructor(private productosService: ProductosService) { }

  ngOnInit() {
    this.productosService.getProducto().subscribe(pro => {this.producto = <any>pro} );

  }

}
