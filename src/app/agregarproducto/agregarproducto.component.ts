import { Component, OnInit } from '@angular/core';
import { Producto } from '../models/producto';
import { ProductosService } from '../services/productos.service';
import { Router, ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-agregarproducto',
  templateUrl: './agregarproducto.component.html',
  styleUrls: ['./agregarproducto.component.css']
})
export class AgregarproductoComponent implements OnInit {
public producto : Producto;
  constructor(private productosService: ProductosService, private activatedRoute: ActivatedRoute, private router: Router)
  {
    this.producto = new Producto(11,'',0);
  }

  ngOnInit() {
  }
  //Metodo para guardar el producto
  saveProducto()
  {
    this.productosService.guardarServicioProducto(this.producto).subscribe(producto =>
      {
        this.router.navigate(['/productos']);        
      },error =>  { console.log(<any>error); }
    );
  }

}
