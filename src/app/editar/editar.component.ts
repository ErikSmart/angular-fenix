import { Component, OnInit } from '@angular/core';
import { Producto } from '../models/producto';
import { ProductosService } from '../services/productos.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})
export class EditarComponent implements OnInit {
public id: any;
public unico: Producto;


  constructor(private productosService: ProductosService, private activatedRoute: ActivatedRoute, private router: Router)
  {
    this.unico = new Producto(0,"",0);
  }

  ngOnInit() {
    this.getDetalle();
  }




  getDetalle()
  {
    //para recorrer los parametros y asignarlo al id
    console.log(this.unico);
      this.activatedRoute.params.forEach((params: Params) => {
        let id = params['id'];
        this.productosService.getDetalle(id)
        .subscribe(arg => {this.saveProducto(); this.unico = <any>arg; });
      });

  }

  //Metodo para guardar el producto
  saveProducto()
  {
    this.activatedRoute.params.forEach((params: Params) => {
      let id = params['id'];
    this.productosService.actulizarProducto(id,this.unico).subscribe(producto =>
      {
        this.router.navigate(['/productos']);
      },error =>  { console.log(<any>error); }
    );
    });
  }


}
